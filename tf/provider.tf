terraform {
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
            version = "~> 0.1.12" 
        }
        kubernetes = {
            source  = "hashicorp/kubernetes"
            version = ">= 2.0.0"
        }
        gitlab = {
            source = "gitlabhq/gitlab"
            version = "16.4.1"
        }
    }
}

provider "vkcs" {
    # Your user account.
    username = "johnkz@inbox.ru"

    # The password of the account
    password = "pc51yiK2"

    # The tenant token can be taken from the project Settings tab - > API keys.
    # Project ID will be our token.
    project_id = "d7a1934237354fe7aa2279158f164bac"
    
    # Region name
    region = "RegionOne"
    
    auth_url = "https://infra.mail.ru:35357/v3/" 
}
/*provider "kubernetes" {
  config_path = "~/.kube/config"
}*/
