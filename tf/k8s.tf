data "vkcs_compute_flavor" "k8s-master-flavor" {
    name = "Standard-2-6"
}

data "vkcs_compute_flavor" "k8s-node-group-flavor" {
 name = "Basic-1-2-20"
}

data "vkcs_kubernetes_clustertemplate" "k8s-template" {
    version = "1.26"
}

resource "vkcs_kubernetes_cluster" "k8s-cluster" {

  depends_on = [
    vkcs_networking_router_interface.k8s,
  ]

  name                = "k8s-cluster-tf"
  cluster_template_id = data.vkcs_kubernetes_clustertemplate.k8s-template.id
  master_flavor       = data.vkcs_compute_flavor.k8s-master-flavor.id
  master_count        = 1
  network_id          = vkcs_networking_network.k8s.id
  subnet_id           = vkcs_networking_subnet.k8s.id
  availability_zone   = "GZ1"

  floating_ip_enabled = true

  labels = {
    calico_ipv4pool   = "10.222.0.0/16"
  }

}

resource "vkcs_kubernetes_node_group" "k8s-node-group" {
  name = "k8s-node-group"
  cluster_id = vkcs_kubernetes_cluster.k8s-cluster.id
  flavor_id = data.vkcs_compute_flavor.k8s-node-group-flavor.id

  node_count = 1
  autoscaling_enabled = false
  min_nodes = 1
  max_nodes = 1

  labels {
        key = "env"
        value = "test"
    }

  labels {
        key = "disktype"
        value = "ssd"
    }

  taints {
        key = "taintkey1"
        value = "taintvalue1"
        effect = "PreferNoSchedule"
    }

  taints {
        key = "taintkey2"
        value = "taintvalue2"
        effect = "PreferNoSchedule"
    }
}

data "vkcs_kubernetes_cluster" "k8s-cluster" {
  cluster_id = vkcs_kubernetes_cluster.k8s-cluster.id
}

output "cluster_config" {
  value = data.vkcs_kubernetes_cluster.k8s-cluster.k8s_config
  description = "IP addresses of the cluster."
}

resource "local_file" "private_key" {
    content  = data.vkcs_kubernetes_cluster.k8s-cluster.k8s_config
    filename = "private_key.pem"
}
