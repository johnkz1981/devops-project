# DevOps project

## Описание проекта

Проект инфрасруктуры облачного решения для разворачивания приложений на примере CMS Wordpress в VK Cloud. 

![](https://gitlab.com/johnkz1981/devops-project/-/raw/main/Doc/Arch.jpeg)

Инфраструктура состоит из: 
- [ ] Инстанс Manager - как единственная точка входа в облако с установленным Gitlab runner;
- [ ] Кластер Kubernetes (3 master-ноды и 2 worker-ноды);
- [ ] Отказоустойчивый кластер MySQL с синхронной репликацией данных для максимальной доступности (3 ноды);
- [ ] Инстанс с файловым хранилищем NFS (для работы со статичными файлами Wordpress);
- [ ] Инстанс Monitoring с Zabbix- сервером;
- [ ] Единая сеть с тремя подсетями для разделения MySQL, NFS, Kubernetes.

**Отказоусточивость** решения достигается за счет бесконечной гибкости и масштабируемости оркестратора Kubernetes, DBaaS-кластера и встроенными балансировщиками нагрузки. 

**Управление инфраструктурой** проекта осуществляется в соответствии с подходом IaC. Код проекта хранится в Gitlab. Для реализации доставки кода инфраструктуры, планируется использовать Gitlab CI. 

**Мониторинг** инфраструктуры осуществляется средствами Zabbix. 

**Бэкап** планируется осуществлять Cloud native  средствами.


## Начальные требования

- [ ] [Аккаунт](https://mcs.mail.ru/) в VC Cloud. Настройте двухфакторную аутентификацию и активируйте доступ по API.
- [ ] Создание ВМ - Manager - из ЛК (Ubuntu 20.04 в любой конфигурации)


## 1. Подключаемся к Manager и выполняем следующие действия:

- [ ] Устанавливаем Terraform
- [ ] Устанавливаем инструмент kubectl
- [ ] Устанавливаем Gitlab runner
- [ ] Устанавливаем Ansible

```
sudo apt upgrade -y
sudo snap install terraform
sudo snap install kubectl
sudo apt  install docker.io  
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner
sudo apt install ansible
git clone https://gitlab.com/johnkz1981/devops-project.git

```
## 2. Установите Gitlab-runner согласно [инструкции](https://docs.gitlab.com/runner/install/)


## 3. Инициализируем Terraform согласно [инструкции](https://mcs.mail.ru/docs/ru/manage/tools-for-using-services/terraform/quick-start#)

```
cd devops-project/
terraform init
terraform plan
terraform apply -auto-approve
```



## Контрольная точка проекта

На данный момент (11.10.2023г) проект реализован на 75%. Terraform поднимает следующую инфраструктуру:

- [ ] Сеть k8s в которой разворачивается инфраструктура;
- [ ] Кластер Kubernetes (3 master-ноды и 2 worker-ноды);
- [ ] Кластер MySQL (3 ноды);
- [ ] Deploy Wordpress с созданием PVC и подключением к БД. Секреты хранятся в gitlab (kuber/kustomization.yaml); 

Wordpress становится доступным по полученному внешнему адресу. 

