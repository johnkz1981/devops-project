resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pass"
  }

  data = {
    password = "SomePass1_"
  }
}

data "gitlab_project_variable" "WORDPRESS_DB_HOST" {
  project = "50795189"
  key     = "WORDPRESS_DB_HOST"
}

resource "kubernetes_service" "wp" {
  metadata {
    name = "wordpress"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    selector = {
      app = "wordpress"
      tier = "frontend"
    }
    port {
      port        = 80
    }
    type = "LoadBalancer"
  }
}

resource "kubernetes_persistent_volume_claim" "wp" {
  metadata {
    name = "wp-pv-claim"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "20Gi"
      }
    }
    storage_class_name = "csi-ceph-hdd-gz1"
  }
}

resource "kubernetes_deployment" "wp" {
  metadata {
    name = "wordpress"
    labels = {
      app = "wordpress"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "wordpress"
        tier = "frontend"
      }
    }
    strategy {
       type = "Recreate" 
    }
    
    template {
      metadata {
        labels = {
          app = "wordpress"
          tier = "frontend"
        }
      }
      spec {
        container {
          image = "wordpress:6.2.1-apache"
          name  = "wordpress"
          env {
               name = "WORDPRESS_DB_HOST"
               value = data.gitlab_project_variable.WORDPRESS_DB_HOST.value
            }
           env  {
              name = "WORDPRESS_DB_PASSWORD"
              value_from {
              secret_key_ref {
              name = "${kubernetes_secret.mysql.metadata.0.name}"
              key = "password"
            }
          }
            }
            env  {
              name = "WORDPRESS_DB_USER"
              value = "testuser"
            }
            port {
               container_port = 80
              name = "wordpress" 
            }
            volume_mount {
               mount_path = "/var/www/html"
              name = "wordpress-persistent-storage" 
            }
        }
        volume {
          name = "wordpress-persistent-storage"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.wp.metadata.0.name
          }
        }
      }
    }
  }
}