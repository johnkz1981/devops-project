variable "public-key-file" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

variable "db-instance-flavor" {
  type    = string
  default = "Standard-2-8-50"
  
}

data "gitlab_project_variable" "network_id" {
  project = "50795189"
  key     = "network_id"
}

data "vkcs_compute_flavor" "db" {
  name = var.db-instance-flavor
}

/*resource "vkcs_compute_keypair" "keypair" {
  name       = "default"
  public_key = file(var.public-key-file)
}*/

resource "vkcs_db_cluster" "db-cluster" {
  name        = "db-cluster"

  availability_zone = "GZ1"
  datastore {
    type    = "galera_mysql"
    version = "8.0"
  }

  cluster_size = 3

  flavor_id   = data.vkcs_compute_flavor.db.id
  #cloud_monitoring_enabled = true

  volume_size = 10
  volume_type = "ceph-ssd"

  network {
    uuid = data.gitlab_project_variable.network_id.value
    #uuid = "35cf44b2-a5cd-403a-a21a-18a7ebb94dfb"
    #subnet_id = vkcs_networking_subnet.subnet_db.id
  }
}

data "vkcs_lb_loadbalancer" "loadbalancer" {
  id = "${vkcs_db_cluster.db-cluster.loadbalancer_id}"
}

data "vkcs_networking_port" "loadbalancer-port" {
  port_id = "${data.vkcs_lb_loadbalancer.loadbalancer.vip_port_id}"
}

output "cluster_ips" {
  value = "${data.vkcs_networking_port.loadbalancer-port.all_fixed_ips}"
  description = "IP addresses of the cluster."
}

resource "gitlab_project_variable" "WORDPRESS_DB_HOST" {
  project   = "50795189"
  key       = "WORDPRESS_DB_HOST"
  value     = data.vkcs_networking_port.loadbalancer-port.all_fixed_ips.0
  protected = false
}

resource "vkcs_db_database" "db-database" {
  name        = "wordpress"
  dbms_id     = vkcs_db_cluster.db-cluster.id
  charset     = "utf8"
  collate     = "utf8_general_ci"
}

resource "vkcs_db_user" "db-user" {
  name        = "testuser"
  password    = "SomePass1_"

  dbms_id     = vkcs_db_cluster.db-cluster.id

  databases   = [vkcs_db_database.db-database.name]
}
